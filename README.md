# Single Responsibility Principle (SRP)
Each class should have only one reason to change
#### SRP analysis framework:
- List all requirements
- Analyze each item in the list to identify potential reasons to change
- Extract functionalities that change due to different reasons into standalone classes
#### Benefits:
- Smaller narrowly focused components
- Encapsulation of the most probable changes within specific components
- Better implementation decisions
- Simpler reuse of existing functionality
- More flexibilty of the codebase as a whole





# Open-Closed Principle (OCP)
The principle depends on stable abstractions (closed for modifications) and modify system's behavior by providing different realizations (open for extension from existent stable abstraction)
#### Objective
- Protect your existent code from a subset of potential future changes that you can predict
#### Limitations:
- Not applicable to *all* classes in your app
- Can't protect from *all possible* changes
- Wrong abstractions can be very hard to fix
#### OCP application framework:
- Apply OCP initially only when you are certain about the nature of future changes
- Monitor changes in requirements and extract useful abstractions as you go
#### Benefits
- Proper utilization of OCP allows you to provide additional business value quckly, safely and at low cost




# Liskov Substitution Princile (LSP)
#### Subtype - subclass or realization which can be substituted for the type it extends or implements
Objects of a supertype shall be replaceable with objects of its subtypes without breaking the application

## Rules applied to pure subtypes
### Signature rule:
- Contravariance of arguments
- Covariance of result
- Exception rule: the exception thrown by a subType should be contained in the set of excetions thrown by super-type
### Methods rule:
- Pre-condition contravariance: preconditions required by methods of a subclass mustn't be stronger than preconditions required by methods of a superclass
- Post-condition covariance: postconditions guaranteed by methods of a subclass mustn't be weaker than postconditions guaranteed by methods of a superclass
### Class property rule:
- Invariants guaranteed by a subclass must include all invariants guaranteed by a superclass
- Constraints enforced by a subclass must include all constraints enforced by a superclass




# Interface Segregation Principle (ISP)
Clients shouldn't be forced to depend on methods that they don't use. In such a case you should extract separate responsibilities in separate interfaces and depend only on required interfaces.

#### Benefits
- Clear picture of what clients *DO* with their services
- Clear picture of what clients *DON'T DO* with their services
- Restrict what clients *CAN DO* with their services
- Segregated interfaces allow to segregate functionality if/when needed (e.g. breaking down god classes)





# Dependency Inversion Principle (DIP)
- High-level modules should not depend on low-level modules. Both should depend on abstractions
- Abstractions should not depend on details. Details should depend on abstractions

#### Benefits
- Protection from future changes in requirements
- Reusability
- Break dependency on external module (and the schedule of another component) 
- Easier integration between modules
- Increased visibility and accountability on organisational level
#### Costs
- Effort to implement
- Effort to change
- Increased code complexity due to indirection
